package de.cwolff.mvcbeispohnedatabinding;

import javafx.scene.control.TextField;

public class FormView implements Observer{
    private Model preisModel;

    private TextField preisOhneMwSt;
    private TextField preisMitMwSt;


    public FormView(Model preisModel) {
        this.preisModel = preisModel;
        initializeGuiElements();
    }

    @Override
    public void update() {
        String[] data = preisModel.getData();
        display(data);
    }

    private void display(String[] data) {
        //setze Daten in Felder
    }

    private void initializeGuiElements(){
        //Felder + Button anlegen & in Stage packen, Event Listeners auf Felder + Button legen

    }
}
