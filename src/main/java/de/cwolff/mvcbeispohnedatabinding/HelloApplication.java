package de.cwolff.mvcbeispohnedatabinding;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {

        Model preisModel = new PreisModel();
        FormView formview = new FormView(preisModel);

        preisModel.addObserver(formview);

        PreisberechnungController preisberechnungController = new PreisberechnungController(preisModel, formview);

        //initialize gui
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 300, 275);
        stage.setScene(scene);
        stage.setTitle("Hello Preis");
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}