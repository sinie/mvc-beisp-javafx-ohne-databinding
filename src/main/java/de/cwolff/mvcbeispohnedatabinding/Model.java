package de.cwolff.mvcbeispohnedatabinding;

import java.util.List;

public abstract class Model {

    private List<Observer> observers;

    public abstract void setData(String[] data);
    public abstract String[] getData();

    public void addObserver(Observer observer){
        observers.add(observer);
    }

    protected void notifyObservers(){
        for (Observer o : observers) {
            o.update();
        }
    }
}
