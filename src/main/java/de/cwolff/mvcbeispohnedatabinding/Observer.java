package de.cwolff.mvcbeispohnedatabinding;

public interface Observer {
    void update();
}
