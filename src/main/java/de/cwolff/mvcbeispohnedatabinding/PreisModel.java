package de.cwolff.mvcbeispohnedatabinding;

public class PreisModel extends Model{

    private double preisMitMwSt;
    private double preisOhneMwSt;
    private final int MWST = 9; //in Prozent

    @Override
    public void setData(String[] data) {
        this.preisMitMwSt = Double.parseDouble(data[0]);
        this.preisOhneMwSt = Double.parseDouble(data[1]);
        notifyObservers();
    }

    @Override
    public String[] getData() {
        return new String[0];
    }
}
