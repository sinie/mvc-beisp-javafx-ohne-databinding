package de.cwolff.mvcbeispohnedatabinding;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.lang.reflect.Array;

public class PreisberechnungController {

    Observer formView;
    Model preisModel;

    public PreisberechnungController(Model preismodel, FormView formview) {
        this.preisModel = preismodel;
        this.formView = formview;
    }

    private void onUserInput(){
        TextField tf = new TextField(); //Felder aus FormView holen
        String alterPreis = "alterPreis"; //Daten von EventListeners der Felder abfragen und mit/ohne MwSt zuordnen
        String neuerPreis = preisBerechnen(alterPreis);
        preisModel.setData(new String[]{alterPreis, neuerPreis});
    }

    private String preisBerechnen(String data) {
        //Berechnung
        return "neuer Preis";
    }
}