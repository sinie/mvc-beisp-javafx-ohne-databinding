module de.cwolff.mvcbeispohnedatabinding {
    requires javafx.controls;
    requires javafx.fxml;


    opens de.cwolff.mvcbeispohnedatabinding to javafx.fxml;
    exports de.cwolff.mvcbeispohnedatabinding;
}